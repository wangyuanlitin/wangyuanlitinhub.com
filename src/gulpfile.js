var gulp = require("gulp");
var sass = require("gulp-sass");
var jade = require("gulp-jade");
var minify = require("gulp-minify");
var inject = require("gulp-inject");

var cheerio = require("cheerio");

paths = {};

paths.scss = ["./stylesheets/*.scss", "./stylesheets/**/*.scss"]
gulp.task("css", function() {
  gulp.src(paths.scss)
    .pipe(sass().on("error", sass.logError))
    .pipe(gulp.dest("../public/css/"))
});

paths.js = ["./javascripts/*.js", "./javascripts/**/*.js"]
gulp.task("js", function() {
  gulp.src(paths.js)
    .pipe(gulp.dest("../public/javascripts/"))
});


paths.jade = ["./jade/blog/time_parse.jade"];
gulp.task("jade", function() {
  gulp.src(paths.jade)
    .pipe(jade({
      pretty: true //不需要压缩
    }))
    .pipe(gulp.dest("../public/html/blog"))
});


paths.vendor = ["./bower_components/**/*", "./lib/**/*"];
gulp.task("vendor", function() {
  gulp.src(paths.vendor).pipe(gulp.dest("../public/vendor/"))
});

var fs = require("fs");

paths.walkpath = "../public/html/blog/*.html";
gulp.task("walk", function() {
  gulp.src("../public/html/blog/index.html")
    .pipe(inject(
      gulp.src(paths.walkpath, { read: false }), {
        transform: function(filepath) {
          var file_name = filepath.split("/").pop();
          if (file_name == "index.html") {
            return
          }

          var temp = "../public/html/blog/" + file_name;

          var data = fs.readFileSync(temp, { encoding: "utf8" });
          var $ = cheerio.load(data);
          var href_text = $(".content .title").text();
          console.log('<li><a href="./' + file_name + '">' + href_text + '</a></li>')
          return '<li><a href="./' + file_name + '">' + href_text + '</a></li>';
        }
      }
    ))
    .pipe(gulp.dest("../public/html/blog/"));
})
gulp.task("default", ["css", "js", "jade", "vendor"]);
